package com.example.ProyectoRepasoSpring;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.example.ProyectoRepasoSpring.modelo.Aula;
import com.example.ProyectoRepasoSpring.modelo.Reserva;
import com.example.ProyectoRepasoSpring.modelo.Usuario;
import com.example.ProyectoRepasoSpring.servicio.ServicioAula;
import com.example.ProyectoRepasoSpring.servicio.ServicioReserva;
import com.example.ProyectoRepasoSpring.servicio.ServicioUsuario;



@SpringBootApplication
public class ProyectoRepasoSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProyectoRepasoSpringApplication.class, args);
		
		
	}
	
	@Bean
	public CommandLineRunner init(	ServicioUsuario servicio, ServicioReserva servicioR, ServicioAula servicioA,
			BCryptPasswordEncoder passwordEncoder ) {
		return args -> {
			
			List<Usuario> usuarios =servicio.findAll();

			for(Usuario usuario :usuarios) {
			String cont =usuario.getPassword();
			usuario.setPassword(passwordEncoder.encode(cont));
			servicio.edit(usuario);
			}

	
			
			Usuario u = new Usuario();
			u.setAdmin(false);
			u.setNombre("Luismi");
			u.setApellidos("Lopez");
			u.setEmail("usuario@usuario.com");
			u.setPassword(passwordEncoder.encode("1234"));
			u.setValidado(true);

			servicio.save(u);

			Usuario a = new Usuario();
			a.setAdmin(true);
			a.setNombre("Angel");
			a.setApellidos("Narajo");
			a.setEmail("admin@admin.com");
			a.setPassword(passwordEncoder.encode("admin"));
			a.setValidado(true);
			
			servicio.save(a);
			
			Aula aula = new Aula();
			aula.setListaReserva(null);
			aula.setNombre("Informatica");
			
			servicioA.save(aula);
			
			
			Reserva r = new Reserva();
			r.setAula(aula);
			r.setFechaDeReserva(LocalDate.now());
			r.setHoraInicio(LocalTime.now());
			r.setUsuario(u);
			
			servicioR.save(r);
			
			
			Reserva re = new Reserva();
			re.setAula(aula);
			re.setFechaDeReserva(LocalDate.of(1997, 7, 27));
			re.setHoraInicio(LocalTime.now());
			re.setUsuario(a);
			
			servicioR.save(re);
		
		
	

		};
		
	}
	
}
