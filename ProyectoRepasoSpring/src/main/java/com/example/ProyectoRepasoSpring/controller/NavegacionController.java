package com.example.ProyectoRepasoSpring.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class NavegacionController {

	@GetMapping("/user")
	public String mostrarInicioUser(Model model) {
		
		return "PlantillaUser";
	}
	
}
