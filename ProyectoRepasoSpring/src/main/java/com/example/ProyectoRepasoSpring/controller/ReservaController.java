package com.example.ProyectoRepasoSpring.controller;

import java.security.Principal;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.ProyectoRepasoSpring.modelo.Pager;
import com.example.ProyectoRepasoSpring.modelo.Reserva;
import com.example.ProyectoRepasoSpring.modelo.Usuario;
import com.example.ProyectoRepasoSpring.servicio.ServicioAula;
import com.example.ProyectoRepasoSpring.servicio.ServicioReserva;
import com.example.ProyectoRepasoSpring.servicio.ServicioUsuario;



@Controller
public class ReservaController {

	//Variables de paginacion
	private static final int BUTTONS_TO_SHOW = 5;
	private static final int INITIAL_PAGE = 0;
	private static final int INITIAL_PAGE_SIZE = 5;
	private static final int[] PAGE_SIZES = {5, 10, 20, 50};

	//Servicio de Reserva
	@Autowired
	private ServicioReserva servicioReserva;
	@Autowired
	private ServicioUsuario servicioUsuario;
	@Autowired
	private ServicioAula servicioAula;
	
	
	//Constructor
	public ReservaController(ServicioReserva servicioReserva) {
		super();
		this.servicioReserva = servicioReserva;
	}

	// METODO PARA BUSCAR RESERVAS PAGINADAS POR FECHA(FALTA REDIRECCIONAR A LA
		// PLANTILLA
		// ADECUADA)
		@GetMapping("/user/buscarReservas")
		public String mostrarReservasUsuario(@RequestParam("pageSize") Optional<Integer> pageSize,
				@RequestParam("page") Optional<Integer> page, @RequestParam("fecha") Optional<LocalDate> fecha, Model model,
				Principal principal) {

			int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);

			int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;

			LocalDate evalFecha = fecha.orElse(null);

			Page<Reserva> reserva = null;
			Usuario usuario = servicioUsuario.buscarUsuarioLogeado(model, principal);
			

			if (evalFecha == null) {
				reserva = servicioReserva.findByUsuario(usuario, PageRequest.of(evalPage, evalPageSize));
			} else {
				reserva = servicioReserva.findByfechaDeReserva(evalFecha, PageRequest.of(evalPage, evalPageSize));
			}

			Pager pager = new Pager(reserva.getTotalPages(), reserva.getNumber(), BUTTONS_TO_SHOW);

			model.addAttribute("reservas", reserva);
			model.addAttribute("selectedPageSize", evalPageSize);
			model.addAttribute("pageSizes", PAGE_SIZES);
			model.addAttribute("pager", pager);

			return "listarReservas";
		}
		
		@GetMapping("/admin/buscarReservas")
		public String mostrarReservasAdmin(@RequestParam("pageSize") Optional<Integer> pageSize,
	            @RequestParam("page") Optional<Integer> page, @RequestParam("fecha") Optional<LocalDate> fecha, Model model) {

	    	
	        int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
	        
	        
	        int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
	        
	        
	        
	        Page<Reserva> reserva = null;
	        
	        	reserva = servicioReserva.findAllPageable(PageRequest.of(evalPage, evalPageSize));
	        

	    
	        Pager pager = new Pager(reserva.getTotalPages(), reserva.getNumber(), BUTTONS_TO_SHOW);
	        
	        
	        
	        model.addAttribute("reservas", reserva);
	      
	        model.addAttribute("selectedPageSize", evalPageSize);
	        model.addAttribute("pageSizes", PAGE_SIZES);
	        model.addAttribute("pager", pager);
	        
	        
	    	return "listarReservasAdmin";
	    }
		
		
		
		
		
		
	// METODO PARA BORRAR UNA RESERVA
	@GetMapping("/borrarReserva/{id}")
	public String borrarReserva(@PathVariable("id") long id) {
		servicioReserva.deleteById(id);
		
		if(servicioUsuario.findById(id).getAdmin()==false) {
			return "redirect:/user/buscarReservas";
			}
			else
			{
				return "redirect:/admin/buscarReservas";
			}
		
	}
	
	// METODO PARA EDITAR UNA RESERVA(FALTA COMPROBAR QUE SI SE EDITA UNA RESERVA,
	// NO SE SOLAPE CON OTRA)
	
	@GetMapping("editarReserva/{id}") 
	public String editarReserva(@PathVariable("id") long id, Model model) {
		Reserva rEditar = servicioReserva.findById(id);
		Boolean reservado;
		
		List<Reserva> rAuxiliar = servicioReserva.findByfechaDeReserva(rEditar.getFechaDeReserva());
		rAuxiliar = servicioReserva.findByaula(rEditar.getAula());
		rAuxiliar = servicioReserva.findByhoraInicio(rEditar.getHoraInicio());
		
		reservado = rAuxiliar.isEmpty();
		
		if (rEditar != null && reservado == false) {
			model.addAttribute("reservas", rEditar);
		}

		
		else {
			//AQUI EL LISTADO DE RESERVAS EN CASO DE QUE NO FUERA NULO
			return "";
			
		} 
	

		//AQUI IRIA EL FORMULARIO
		return "";
	}
	
	@PostMapping("editarReserva/submit")
	public String editarReservaProcesar(@ModelAttribute("reserva") Reserva r, @PathVariable("id") long id, Model model) {
		
		servicioReserva.edit(r);
		
		if(servicioUsuario.findById(id).getAdmin()==false) {
		return "redirect:/user/buscarReservas";
		}
		else
		{
			return "redirect:/user/buscarReservas";
		}
		
	}
	@GetMapping("/user/selecfechahora")
	public String buscarReservas(Model model) {
		
		model.addAttribute("hora", servicioReserva.getRangoHorario());
		model.addAttribute("reserva", new Reserva());
		
		return "selecfechahora";
		
	}
	@PostMapping("/user/selecaula")
	public String formReserva(@ModelAttribute ("reserva") Reserva r,Model model) {
		
		model.addAttribute("aulas",servicioReserva.aulasLibres(r.getHoraInicio(), r.getFechaDeReserva(), servicioAula));
		model.addAttribute("reserva1", new Reserva());
		
		return "formaula";
		
	}
	
	@PostMapping("/user/submitreserva")
	public String submitReserva(@ModelAttribute ("reserva1") Reserva r, Model model , Principal principal) {
		
		 Usuario u = servicioUsuario.buscarUsuarioLogeado(model, principal);
		r.addUsuario(u);
		r.addAula(r.getAula());
		servicioReserva.save(r);
		return "redirect:/user/buscarReservas";
	}
	
	

}
