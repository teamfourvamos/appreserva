package com.example.ProyectoRepasoSpring.controller;

import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.ProyectoRepasoSpring.modelo.Pager;
import com.example.ProyectoRepasoSpring.modelo.Usuario;
import com.example.ProyectoRepasoSpring.servicio.ServicioUsuario;




@Controller
public class UsuarioController {
	
	//Variables de paginación
	private static final int BUTTONS_TO_SHOW = 5;
	private static final int INITIAL_PAGE = 0;
	private static final int INITIAL_PAGE_SIZE = 5;
	private static final int[] PAGE_SIZES = {5, 10, 20, 50};
	
	@Autowired
	ServicioUsuario servicioUsuario;
	
	@Autowired
	HttpSession session;

	@GetMapping("/login")
	public String mostrarForm(Model model) {
		
		model.addAttribute("usuario", new Usuario());
		
		
		return "login";

	}
	@PostMapping("/submit")
	public String submit(@ModelAttribute ("usuario")Usuario u,BCryptPasswordEncoder passwordEncoder) {
		
		System.out.println(u.getEmail());
		u.setPassword(passwordEncoder.encode(u.getPassword()));
		u.setAdmin(false);
		u.setValidado(false);
		servicioUsuario.save(u);
		
		return "redirect:/login";

	}
	
	@GetMapping("/admin/validaciones")
	public String showUsuarioPage(@RequestParam("pageSize") Optional<Integer> pageSize,
			@RequestParam("page") Optional<Integer> page, @RequestParam("nombre") Optional<String> nombre,
			Model model) {

		

		// Evalúa el tamaño de página. Si el parámetro es "nulo", devuelve
		// el tamaño de página inicial.
		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);

		// Calcula qué página se va a mostrar. Si el parámetro es "nulo" o menor
		// que 0, se devuelve el valor inicial. De otro modo, se devuelve el valor
		// del parámetro decrementado en 1.
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;

		String evalNombre = nombre.orElse(null);

		Page<Usuario> usuarios = null;

		if (evalNombre == null) {
			usuarios = servicioUsuario.encontrarUsuarioNoValidado(PageRequest.of(evalPage, evalPageSize));
		} else {
			usuarios = servicioUsuario.findByNombreContainingIgnoreCase(evalNombre, PageRequest.of(evalPage, evalPageSize));
		}

		
		Pager pager = new Pager(usuarios.getTotalPages(), usuarios.getNumber(), BUTTONS_TO_SHOW);

		model.addAttribute("usuario", usuarios);
		model.addAttribute("selectedPageSize", evalPageSize);
		model.addAttribute("pageSizes", PAGE_SIZES);
		model.addAttribute("pager", pager);

		
		return "pages/Validaciones";
	}
	
	@GetMapping("/admin/validarUsuario/{id}")
	public String validarUsuario(@PathVariable("id") Usuario u) {
		servicioUsuario.validarUsuario(u);
		return "redirect:/admin/validaciones";
	}
	
	@GetMapping("/admin/rechazarUsuario/{id}")
	public String rechazarUsuario(@PathVariable("id") Usuario u) {
		servicioUsuario.rechazarrUsuario(u);
		return "redirect:/admin/validaciones";
	}
	
	

	
}
