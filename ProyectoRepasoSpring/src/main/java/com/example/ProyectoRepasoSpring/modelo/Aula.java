package com.example.ProyectoRepasoSpring.modelo;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.Data;

@Data
@Entity
public class Aula {

	//Atributos
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String nombre;
	@OneToMany
	private List<Reserva> listaReserva;
	
	//Constructor
	public Aula(String nombre, Boolean esReservada, List<Reserva> listaReserva) {
		super();
		this.nombre = nombre;
		this.listaReserva = listaReserva;
	}
	public Aula() {
		
	}
	

	



	
	
	
	
}
