package com.example.ProyectoRepasoSpring.modelo;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;

@Data
@Entity
public class Reserva {

	// atributos
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	private LocalTime horaInicio;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate fechaDeReserva;
	

	@ManyToOne
	private Aula aula;

	@ManyToOne
	private Usuario usuario;

	// constructor
	public Reserva(long id, LocalTime horaInicio, LocalDate fechaDeReserva, Aula aula, Usuario usuario) {
		super();
		this.id = id;
		this.horaInicio = horaInicio;
		this.fechaDeReserva = fechaDeReserva;
		this.aula = aula;
		this.usuario = usuario;
	}

	public Reserva() {
		// TODO Auto-generated constructor stub
	}

	public long getTiempoRestante() {
		
		return ChronoUnit.DAYS.between(LocalDate.now(), fechaDeReserva);
	}
	/**
	 * Método auxiliar para el tratamiento bidireccional de la asociación. 
	 * Añade a Aula este Reserva y setea el Aula en Reserva
	 * @param e lote añadir
	 */
	public void addAula(Aula a) {
		this.setAula(a);
		a.getListaReserva().add(this);
		
	}
	
	/**
	 *  Método auxiliar para el tratamiento bidireccional de la asociación.
	 *  Añade al usuario reserva y a reserva le asigna ese usuario.
	 * @param e equipo a añadir
	 */
	public void addUsuario(Usuario u) {
		this.setUsuario(u);
		u.getListaReserva().add(this);
	}
	

}
