package com.example.ProyectoRepasoSpring.modelo;


import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@Entity
public class Usuario {

	//Atributos
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	private String nombre;
	private String apellidos;
	private String email;
	private String password;
	private Boolean admin;
	private Boolean validado;
	
	@EqualsAndHashCode.Exclude
	@ToString.Exclude
	@OneToMany(mappedBy="usuario" ,cascade=CascadeType.ALL) 
	private List<Reserva> listaReserva;

	
	//Constructor
	public Usuario(String nombre, String apellidos, String email, String password, Boolean admin, Boolean validado
			, List<Reserva> listaReserva) {
		super();
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.email = email;
		this.password = password;
		this.admin = admin;
		this.validado = validado ;
		this.listaReserva = listaReserva;

	}
	
	public Usuario() {
		
	}

	



}
