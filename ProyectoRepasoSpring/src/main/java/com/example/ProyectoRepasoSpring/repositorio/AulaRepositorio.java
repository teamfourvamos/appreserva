package com.example.ProyectoRepasoSpring.repositorio;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.ProyectoRepasoSpring.modelo.Aula;

@Repository
public interface AulaRepositorio extends JpaRepository<Aula,Long> {

	public  Page<Aula> findByNombreContainingIgnoreCase(String nombre, Pageable pageable);
	
	public  List<Aula> findByNombreContainingIgnoreCase(String nombre);
	
	public Page<Aula> findAll(Pageable pageable);
	
	
}
