package com.example.ProyectoRepasoSpring.repositorio;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.ProyectoRepasoSpring.modelo.Aula;
import com.example.ProyectoRepasoSpring.modelo.Reserva;
import com.example.ProyectoRepasoSpring.modelo.Usuario;

@Repository
public interface ReservaRepositorio extends JpaRepository<Reserva,Long> {
	
	
	public List<Reserva> findByfechaDeReserva(LocalDate fecha);
	
	public  Page<Reserva> findByhoraInicio(LocalTime horaInicio, Pageable pageable);
	
	public  List<Reserva> findByhoraInicio(LocalTime horaInicio);
	
	public  Page<Reserva> findByfechaDeReserva(LocalDate fecha, Pageable pageable);
	
	public Page<Reserva> findByfechaDeReserva(String fecha, Pageable pageable);
	
	public List<Reserva> findByaula(Aula aula);
	
	public Page<Reserva> findByUsuario(Usuario logeado,Pageable pageable);
	
	@Query(value = "SELECT DATEDIFF(DAY,FECHA_INICIO,FECHA_FINAL) FROM RESERVA where ID = (?1)", nativeQuery = true )
	public long calcularTiempoRestante(long id);
	
	

}