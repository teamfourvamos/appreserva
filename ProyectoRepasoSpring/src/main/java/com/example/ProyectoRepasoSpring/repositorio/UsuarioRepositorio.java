package com.example.ProyectoRepasoSpring.repositorio;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.ProyectoRepasoSpring.modelo.Usuario;


@Repository
public interface UsuarioRepositorio extends JpaRepository<Usuario,Long> {

	Usuario findFirstByEmail(String email);


@Query("select u from Usuario u where u.validado = false")
Page<Usuario> encontrarUsuarioNoValidado(Pageable pageable);


@Query("select u from Usuario u where u.validado = false")
Page<Usuario> findByNombreContainingIgnoreCase(String nombre, Pageable pageable);


}

