/**
 * 
 */
package com.example.ProyectoRepasoSpring.seguridad;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.User.UserBuilder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.example.ProyectoRepasoSpring.modelo.Usuario;
import com.example.ProyectoRepasoSpring.servicio.ServicioUsuario;


/**
 * @author Juanma
 *
 */
@Service("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

	ServicioUsuario usuarioServicio;

	public UserDetailsServiceImpl(ServicioUsuario servicio) {
		this.usuarioServicio = servicio;
	}

	/**
	 * Metodo que contruye la sesion de usuario por el email del trabajador
	 * conectado.
	 */
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		Usuario usuario = usuarioServicio.buscarPorEmail(username);

		UserBuilder userBuilder = null;
		
		if (usuario != null) {
			if (usuario.getValidado()) {
				userBuilder = User.withUsername(usuario.getEmail());
				userBuilder.disabled(false);
				userBuilder.password(usuario.getPassword());
				if (usuario.getAdmin()) {
					// Este caso indica que un ADMIN también puede hacer todo lo que hace un USER
					userBuilder.authorities(new SimpleGrantedAuthority("ROLE_ADMIN"));				
				} else {
					userBuilder.authorities(new SimpleGrantedAuthority("ROLE_USER"));				
				}
			} else {
				throw new UsernameNotFoundException("Usuario no encontrado");
			}
			
		} else {
			throw new UsernameNotFoundException("Usuario no encontrado");
		}
			
		return userBuilder.build();
		
	}
		
}
