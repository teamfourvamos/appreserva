package com.example.ProyectoRepasoSpring.servicio;


import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.ProyectoRepasoSpring.modelo.Aula;
import com.example.ProyectoRepasoSpring.repositorio.AulaRepositorio;
import com.example.ProyectoRepasoSpring.servicios.base.BaseService;

@Service
public class ServicioAula extends BaseService<Aula, Long, AulaRepositorio > {

	private final AulaRepositorio aulaRepositorio;
	
	
	public ServicioAula(AulaRepositorio aulaRepositorio) {
		super();
		this.aulaRepositorio = aulaRepositorio;
	}


	public  List<Aula> findByNombreContainingIgnoreCase(String nombre){
		
		return aulaRepositorio.findByNombreContainingIgnoreCase(nombre); 
	}
	
	public  Page<Aula> findByNombreContainingIgnoreCase(String nombre, Pageable pageable){
		
		return aulaRepositorio.findByNombreContainingIgnoreCase(nombre, pageable);
	}
	
	public Page<Aula> findAll(Pageable pageable){
		
		return aulaRepositorio.findAll(pageable);
	}


	
	
}
