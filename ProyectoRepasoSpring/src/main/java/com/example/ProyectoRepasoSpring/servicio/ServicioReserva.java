package com.example.ProyectoRepasoSpring.servicio;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.ProyectoRepasoSpring.modelo.Aula;
import com.example.ProyectoRepasoSpring.modelo.Reserva;
import com.example.ProyectoRepasoSpring.modelo.Usuario;
import com.example.ProyectoRepasoSpring.repositorio.ReservaRepositorio;
import com.example.ProyectoRepasoSpring.servicios.base.BaseService;




@Service 
public class ServicioReserva extends BaseService<Reserva, Long, ReservaRepositorio > {

	private List<LocalTime> rangoHorario = Arrays.asList(LocalTime.parse( "08:00" ),LocalTime.parse( "09:00" ),LocalTime.parse( "10:00" ),LocalTime.parse( "11:30" ),LocalTime.parse( "12:30" ),LocalTime.parse( "13:30" ));

	private final ReservaRepositorio reservaRepositorio;
	
	
	
	public List<Reserva> reservasFechaHora(LocalTime hora,LocalDate fecha) {
		List<Reserva> res = reservaRepositorio.findAll();
		List<Reserva> resF  = new ArrayList<>();
		 for (Reserva reserva : res) {
			 if(reserva.getHoraInicio().equals(hora)){
				if(reserva.getFechaDeReserva().equals(fecha)) {
					resF.add(reserva);
				}
			 }
		 }
		
		return resF;	
	}
	
	public List<Aula> aulasLibres(LocalTime hora,LocalDate fecha,ServicioAula servicioaula){
		
		List<Aula> auF  = servicioaula.findAll();
		
		for(Reserva r : reservasFechaHora(hora, fecha)) {
			for(Iterator<Aula> iterator = auF.iterator();iterator.hasNext();) {
				Aula aula = iterator.next();
				if(r.getAula().equals(aula)) {
					iterator.remove();
				}
			}
		}
		
		
		return auF;
		
	}
	
	public ServicioReserva(ReservaRepositorio reservaRepositorio) {
		this.reservaRepositorio = reservaRepositorio;
	}
	
	public Page<Reserva> findAllPageable(Pageable pageable) {
		return reservaRepositorio.findAll(pageable);
	}
	
	public  List<Reserva> findByhoraInicio(LocalTime horaInicio){
		
		return reservaRepositorio.findByhoraInicio(horaInicio);
		
	}
	
public  Page<Reserva> findByhoraInicio(LocalTime horaInicio, Pageable pageable){
	
	return reservaRepositorio.findByhoraInicio(horaInicio, pageable);
	
}
	
	public  Page<Reserva> findByfechaDeReserva(LocalDate fecha, Pageable pageable){
		
		return reservaRepositorio.findByfechaDeReserva(fecha, pageable);
		
	}
	
public  Page<Reserva> findByfechaDeReserva(String fecha, Pageable pageable){
		
		return reservaRepositorio.findByfechaDeReserva(fecha, pageable);
		
	}

public List<Reserva> findByfechaDeReserva(LocalDate fechaDeReserva) {
	
	return reservaRepositorio.findByfechaDeReserva(fechaDeReserva);
}

public List<Reserva> findByaula(Aula aula) {

	return reservaRepositorio.findByaula(aula);
}

public Page<Reserva> findByUsuario(Usuario logeado, Pageable pageable){
	
	return reservaRepositorio.findByUsuario(logeado, pageable);
}


public List<LocalTime> getRangoHorario() {
	return rangoHorario;
}

public void setRangoHorario(List<LocalTime> rangoHorario) {
	this.rangoHorario = rangoHorario;
}

public long calcularTiempoRestante(long id) {
	return reservaRepositorio.calcularTiempoRestante(id);
}

}
