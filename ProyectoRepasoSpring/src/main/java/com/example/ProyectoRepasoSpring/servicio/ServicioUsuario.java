package com.example.ProyectoRepasoSpring.servicio;


import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import com.example.ProyectoRepasoSpring.modelo.Usuario;
import com.example.ProyectoRepasoSpring.repositorio.UsuarioRepositorio;
import com.example.ProyectoRepasoSpring.servicios.base.BaseService;



@Service
public class ServicioUsuario extends BaseService<Usuario, Long, UsuarioRepositorio> {
	
	@Autowired
	private final UsuarioRepositorio usuarioRepositorio;
	
	public ServicioUsuario(UsuarioRepositorio usuarioRepositorio) {
		super();
		this.usuarioRepositorio = usuarioRepositorio;
	}
	
	
	/**
	 * Encuentra al primero de los usuarios por su email
	 * @param email
	 * */
	public Usuario buscarPorEmail(String email) {
		return repositorio.findFirstByEmail(email);
	}
	
	public Usuario buscarUsuarioLogeado(Model model, Principal principal) {

		if (principal != null) {
			String email = principal.getName();
			Usuario usuario = buscarPorEmail(email);
			model.addAttribute("usuario", usuario);

			return usuario;
		} else {
			return null;
		}
		
	}

public Page<Usuario> findAll(Pageable pageable){
		
		return usuarioRepositorio.findAll(pageable);
	}

public Page<Usuario> encontrarUsuarioNoValidado(Pageable pageable) {
	return usuarioRepositorio.encontrarUsuarioNoValidado(pageable);
}


public Page<Usuario> findByNombreContainingIgnoreCase(String nombre, Pageable pageable){
	
	return usuarioRepositorio.findByNombreContainingIgnoreCase(nombre, pageable);
}


public void validarUsuario(Usuario u) {
	
	u.setValidado(true);
	edit(u);

	
}

public void rechazarrUsuario(Usuario u) {
	
	usuarioRepositorio.delete(u);

	
}



}
