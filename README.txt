La aplicaci�n ha sido dise�ada y creada para cumplir la funci�n de aplicaci�n de gesti�n
de un colegio, concretamente para gestionar las reservas de las diferentes aulas del mismo.

Para acceder a la aplicaci�n desde el perfil de usuario simplemente ha de loguearse si ya
posee una cuenta o bien registrarse dentro de la misma, en el caso de que se haya registrado
ha de esperar a que un administrador de la aplicac�on le valide su acceso, despues de esto
podra acceder a funciones tales como realizar una reserva o listar las aulas que existen.

En el caso de que usted sea un administrador, podra acceder a funcionalidades tales como
borrar reservas, editar y crear aulas o validar a los usuarios que se hayan registrado en
la aplicaci�n

Toda la aplicaci�n cuenta con seguridad gestionada mediante Spring Security y no podra acceder
a las zonas no permitidas, es decir, si usted es un usuario no podra acceder a la gestion de aulas
del administrador

La aplicaci�n se presenta en dos idiomas diferentes, Ingl�s y Espa�ol(Espa�a), podra alternar
entre ambos siempre que este logueado en la aplicaci�n.

Autor�a de la aplicac�on:
-Antonio Duran
-Ricardo Mej�as
-Juan Manuel Gracia
-Juan Antonio Lagos

Autor�a del archivo README:
-Ricardo Mej�as